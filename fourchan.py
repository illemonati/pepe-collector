import basc_py4chan
import os
import shutil
import mimetypes
import requests


def download_images_from_thread(board_name, thread_id, download_dir):
    if not os.path.exists(download_dir):
        os.makedirs(download_dir)

    board = basc_py4chan.Board(board_name)
    if not board.thread_exists(thread_id):
        raise Exception(f"Thread {thread_id} not found in board")

    thread = board.get_thread(thread_id)

    for file_url in thread.files():
        file_name = os.path.basename(file_url)
        mtype = mimetypes.guess_type(file_name)
        if mtype[0] not in ('image/jpeg', 'image/png'):
            continue
        resp = requests.get(file_url)
        p = os.path.join(download_dir, file_name)

        with open(p, 'wb+') as file:
            file.write(resp.content)
