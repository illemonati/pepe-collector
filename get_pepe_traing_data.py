import json
import discord
import os
import requests

DOWNLOAD_DIR = "./pepe-training-data/"
if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)


async def process_collect(message: discord.Message):
    collected = 0
    async for msg in message.channel.history(limit=100000,  oldest_first=True):
        for attachment in msg.attachments:
            print(attachment.content_type)
            if attachment.content_type not in ('image/jpeg', 'image/png'):
                continue
            resp = requests.get(attachment.url)
            p = os.path.join(DOWNLOAD_DIR, attachment.filename)
            with open(p, 'wb+') as file:
                file.write(resp.content)
            collected += 1
    print(collected)


class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        if message.author != self.user:
            return

        if message.content == 'ping':
            await message.channel.send('pong')

        if message.content.startswith('collect'):
            await process_collect(message)


client = MyClient()
client.run(os.environ.get('PEPE_DISCORD_TOKEN'), bot=False)
