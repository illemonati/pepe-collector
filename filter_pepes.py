import tensorflow as tf
import mimetypes
import os
import shutil
import matplotlib.pyplot as plt

model = tf.keras.models.load_model('pepe_model.h5')


def filter_for_pepes(raw_path, pepe_path):
    pepes = 0
    if not os.path.exists(pepe_path):
        os.makedirs(pepe_path)
    for file_name in os.listdir(raw_path):
        mtype = mimetypes.guess_type(file_name)
        if mtype[0] not in ('image/jpeg', 'image/png'):
            continue
        path = os.path.join(raw_path, file_name)
        image = tf.keras.preprocessing.image.load_img(
            path, target_size=(32, 32))
        # plt.imshow(image)
        # plt.show()
        input_arr = tf.keras.preprocessing.image.img_to_array(
            image).reshape((1, 32, 32, 3))
        predictions = model.predict(input_arr)
        if predictions[0][0] > 1e-6:
            continue
        print(predictions)
        pepes += 1
        out_file_path = os.path.join(pepe_path, file_name)
        shutil.copyfile(path, out_file_path)
    return pepes
